﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public static class Ulti
{
    private static RectTransform rectCanvas;
    public static Vector2 ConvertScreenPositonToCanvas(Vector2 screenPos, CanvasScaler canvasScaler, float screenWid, float screenHei)
    {
        Vector2 result = Vector2.zero;
        RectTransform rectCanvas = canvasScaler.GetComponent<RectTransform>();
        result.x = screenPos.x / screenWid * rectCanvas.sizeDelta.x;
        result.y = screenPos.y / screenHei * rectCanvas.sizeDelta.y;
        return result;
    }

    public static Color ConvertHexStringToColor(string hexString)
    {
        hexString = hexString.ToUpper();
        float colorR = Convert.ToInt32(hexString.Substring(0, 2), 16) / 255f;
        float colorG = Convert.ToInt32(hexString.Substring(2, 2), 16) / 255f;
        float colorB = Convert.ToInt32(hexString.Substring(4, 2), 16) / 255f;
        if (hexString.Length == 8)
        {
            return new Color(colorR, colorG, colorB, Convert.ToInt32(hexString.Substring(6, 2), 16) / 255f);
        }
        else
        {
            return new Color(colorR, colorG, colorB, 1);
        }
    }

    public static Color ConvertPercentStringToColor(float red, float green, float blue, float alpha)
    {
        return new Color(red, green, blue, alpha);

    }

    public static string AddStringColor(object contend, string colorCode)
    {
        string result = "<color=#" + colorCode + ">" + contend.ToString() + "</color>";
        return "<color=#" + colorCode + ">" + contend.ToString() + "</color>";
    }

    public static float RandomRange(float minValue, float maxValue)
    {
        return UnityEngine.Random.Range(minValue, maxValue);
    }
    public static int RandomRange(int minValue, int maxValue)
    {
        return UnityEngine.Random.Range(minValue, maxValue);
    }

    public static string convertCountDownTimeToText(int totalSecond)
    {
        int days = totalSecond / 86400;
        int hours = totalSecond / 3600;
        int minutes = totalSecond / 60 - hours * 60;
        int seconds = totalSecond % 60;
       
        string daysStr = ((days > 9) ? days.ToString() : ("0" + days)) + "d ";
        string hoursStr = ((hours > 9) ? hours.ToString() : ("0" + hours)) + ":";
        string minutesStr = ((minutes > 9) ? minutes.ToString() : ("0" + minutes))  + ":";
        string secondsStr = ((seconds > 9) ? seconds.ToString() : ("0" + seconds)) + "";
        if (days != 0)
        {
            minutesStr = "";
            secondsStr = "";
        }
        else if (hours != 0)
        {
            daysStr = "";
            secondsStr = "";
        }
        else if (minutes != 0)
        {
            daysStr = "";
            hoursStr = "";
        }
        else
        {
            daysStr = "";
            hoursStr = "";
        }
        return daysStr + hoursStr + minutesStr + secondsStr;
    }
}
