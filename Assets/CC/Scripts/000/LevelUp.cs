﻿using DG.Tweening;
using Sfs2X.Entities.Data;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelUp : MonoBehaviour
{
    [SerializeField]
    GameObject levelup;
    [SerializeField]
    Text txtLevel, txtTitle, txtReceive, txtChipBonus;
    [SerializeField]
    Image BG, BorderReceive;
   
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
   
    public void OnModeratorMessage(string _msg)
    {
        StartCoroutine(UpdateInfoLevel(_msg));
        //{"cur_level":1,"receive_exp":3,"max_exp":20,"is_level_up":1,"exp":3,"min_exp":0}
        
    }

    private IEnumerator UpdateInfoLevel(string _msg)
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == GameScene.BaiCaoScence.ToString())
        {
            yield return new WaitForSeconds(10f);
        }
        Debug.Log("====OnModeratorMessage======" + _msg);
        JSONNode node = JSONNode.Parse(_msg);
        MyInfo.CUR_LEVEL = node["cur_level"].AsInt;
        MyInfo.CUR_EXP = node["exp"].AsInt;
        MyInfo.MIN_EXP = node["min_exp"].AsInt;
        MyInfo.MAX_EXP = node["max_exp"].AsInt;
        //View.GetTxtLevel().text = node["cur_level"];
        if (node["is_level_up"].AsInt == 1)
        {
            BG.gameObject.SetActive(true);
            txtLevel.gameObject.SetActive(true);
            txtTitle.gameObject.SetActive(true);
            txtReceive.gameObject.SetActive(false);
            BorderReceive.gameObject.SetActive(false);
            txtChipBonus.gameObject.SetActive(true);
            txtChipBonus.text = "+" + node["chip_bonus"];
            txtLevel.text = node["cur_level"];
        }
        else
        {
            txtReceive.text = "+" + node["receive_exp"] + " EXP";
            txtReceive.gameObject.SetActive(true);
            BorderReceive.gameObject.SetActive(true);
            levelup.SetActive(true);
            BG.gameObject.SetActive(false);
            txtLevel.gameObject.SetActive(false);
            txtTitle.gameObject.SetActive(false);
            txtChipBonus.gameObject.SetActive(false);
        }
        StartCoroutine(LevelUpUser());
    }

    private IEnumerator LevelUpUser()
    {
        levelup.transform.DOScale(1f, 0f);
        levelup.SetActive(true);
        levelup.transform.DOScale(2f, 0.5f).SetEase(Ease.OutExpo);
        yield return new WaitForSeconds(0.5f);
        levelup.transform.DOScale(1f, 0.5f);
        StartCoroutine(HideLevelup());
    }

    private IEnumerator HideLevelup()
    {
        yield return new WaitForSeconds(3.0f);
        levelup.SetActive(false);
    }
}
