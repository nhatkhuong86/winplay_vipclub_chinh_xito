﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemBuyBorderAvatar : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    ShopBorderAvatar Shop;
    [SerializeField]
    Text TxtVip, TxtPrice;
    
    [SerializeField]
    Image ImgBor, ImgIcon;
    [SerializeField]
    Sprite TuiVang, KimCuong;
    Button MyButton;
    public int My_Id_Border = -1;
    string Id_Border;
    bool _ByGem = false;
    Sprite SprBor;

    string _Gia;
    string _Vip;

    public void Init(string id, string Vip, string Gia, Sprite spr, bool ByGem = false)
    {
        Id_Border = id;
        My_Id_Border = int.Parse(id);
        TxtVip.text = "VIP " + Vip;
        _ByGem = ByGem;
        _Gia = Gia;
        _Vip = Vip;

        if (ByGem == true)
        {
            TxtPrice.text = "<color=#49EEFDFF>" + Gia + "</color>";
            ImgIcon.sprite = KimCuong;
        }
        else
        {
            long t = long.Parse(Gia);
            string gia = Utilities.GetStringMoneyByLong(t);
            TxtPrice.text = gia;
            ImgIcon.sprite = TuiVang;
        }
        ImgBor.sprite = spr;
        SprBor = ImgBor.sprite;
        MyButton = this.gameObject.GetComponent<Button>();
        MyButton.onClick.RemoveAllListeners();
        MyButton.onClick.AddListener(OnClick);
    }

    public void OnClick()
    {      
        ShopBorderAvatar.Instance.MoPanelMua(Id_Border, _Vip, _Gia, _ByGem);
    }

   
}
