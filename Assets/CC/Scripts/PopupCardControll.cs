﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using BaseCallBack;

public class PopupCardControll : MonoBehaviour
{

    [SerializeField]
    private InputField CardPrice;
    [SerializeField]
    private Dropdown CardType;
    [SerializeField]
    private InputField Pin;
    [SerializeField]
    private InputField Serial;
    [SerializeField]
    private Button BtnNap, btnClosePopupCard;
    [SerializeField]
    private Text Warning, PriceConfrim;
    private string Price;
    [SerializeField]
    GameObject PanelConfrim, panelProcessingPayment;
    [SerializeField]
    EvenDapHeo EvenDH;
    [SerializeField]
    PopupShopManager Shop;
    int Id_pig = 0;
    onCallBackString callBack;
    public string HammerType = "iron_hammer";

    //iron_hammer, silver_hammer, gold_hammer)

    public void Init(string price, int Id_Pig, onCallBackString _callBack = null)//Id_Pig 2,3 la` mua bua dap heo ---- 4 là mua qu? bói toán ---- 15 la dành cho shop ---- 20 Là mua Gem
    {
        callBack = _callBack;
        //string.Format("{0:n0}", int.Parse(price));
        CardPrice.text = price;
        Price = price;
        Id_pig = Id_Pig;
        Pin.contentType = InputField.ContentType.IntegerNumber;
        Pin.characterLimit = 20;
        Serial.contentType = InputField.ContentType.IntegerNumber;
        Serial.characterLimit = 20;
        Warning.text = "";
        BtnNap.onClick.RemoveListener(BtnNapClick);
        BtnNap.onClick.AddListener(BtnNapClick);
        if (btnClosePopupCard != null)
        {
            btnClosePopupCard.onClick.RemoveListener(ClosePopupCard);
            btnClosePopupCard.onClick.AddListener(ClosePopupCard);
        }

        setDataCardCombobox();
        gameObject.SetActive(true);
        if (Id_Pig == 2)
        {
            HammerType = "silver_hammer";
        }
        else if (Id_Pig == 3)
        {
            HammerType = "gold_hammer";
        }






    }


    public void BtnNapClick()
    {
        bool check = CheckInput();
        if (check == true && Id_pig == 4)
        {
            MuaQueBoi();
        }
        else if (check == true && Id_pig == 20)
        {
            BuyGem();
        }
        else if (check == true && Id_pig != 15)
        {
            BuyTurn();
        }
        else if (check == true && Id_pig == 15)
        {
            BuyTurn(false);
        }
       


    }

    public void ClosePanelConfrim()
    {
        PriceConfrim.text = "";
        PanelConfrim.SetActive(false);
    }


    private bool CheckInput()
    {
        if (Pin.text.Length <= 6)
        {
			Warning.text = "Mã nạp thẻ không hợp lệ!";
            return false;
        }
        if (Serial.text.Length <= 6)
        {
			Warning.text = "Mã serial không hợp lệ!";
            return false;
        }
        return true;
    }



    public void ShowPopupCard()
    {
        setDataCardCombobox();
        gameObject.SetActive(true);
    }

    internal void Init(string pRICE_THE_CAO, int v, object buyXamTheCaoComplete)
    {
        throw new NotImplementedException();
    }

    public void ClosePopupCard()
    {
        Warning.gameObject.SetActive(false);
        gameObject.SetActive(false);
        Price = "";
        Pin.text = "";
        Serial.text = "";
        CardType.value = 0;
        Warning.text = "";
    }

    public void buyPackByMobieCard(string price)//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        CardPrice.text = price;

        ShowPopupCard();
    }




    private void setDataCardCombobox()
    {
        //Debug.LogError(name + "Mobifone::::::" + GameHelper.dicConfig["Mobifone"]);
        //Debug.LogError(name + "Viettel::::::" + GameHelper.dicConfig["Viettel"]);
        //Debug.LogError(name + "Vinaphone::::::" + GameHelper.dicConfig["Vinaphone"]);

        bool haveViettel = true;
        bool haveMobi = true;
        bool haveVina = true;
        if (int.Parse(GameHelper.dicConfig["Mobifone"]) == 0 || !GameHelper.CardPayment.cardMobi)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Mobifone")
                {
                    CardType.options.RemoveAt(i);
                    haveMobi = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Viettel"]) == 0 || !GameHelper.CardPayment.cardVietel)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Viettel")
                {
                    CardType.options.RemoveAt(i);
                    haveViettel = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Vinaphone"]) == 0 || !GameHelper.CardPayment.cardVina)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Vinaphone")
                {
                    CardType.options.RemoveAt(i);
                    haveVina = false;
                    break;
                }
            }
        }
        if (!haveMobi)
        {
            CardType.captionText.text = "Viettel";
            if (!haveViettel)
            {
                CardType.captionText.text = "Vinaphone";
                if (!haveVina)
                {
                    CardType.captionText.text = "";
                }
            }

        }
    }



    public void MuaQueBoi()
    {
        Debug.Log("mua que boi ne");
        string price = Price.ToString();
        string Type = CardType.captionText.text.ToLower();
        string pin = Pin.text;
        string serial = Serial.text;
        API.Instance.RequestMuaQueBoi(Type, price, pin, serial, MuaQueBoiComplete);
        panelProcessingPayment.gameObject.SetActive(true);
    }
    private void MuaQueBoiComplete(string s)
    {
        panelProcessingPayment.gameObject.SetActive(false);
        callBack(s);
        ClosePopupCard();
    }

    public void BuyTurn(bool BuyHammer = true)
    {
        //call service
        if (BuyHammer == true)
        {
            string price = Price.ToString();
            string Type = CardType.captionText.text.ToLower();
            string pin = Pin.text;
            string serial = Serial.text;
            Debug.Log("gia = " + price + " --------loai the = " + Type + " --------pin = " + pin + " --------seri = " + serial);
            API.Instance.RequestBuyPigHammerByCard(HammerType, Type, price, pin, serial, BuyTurnComplete);
            panelProcessingPayment.gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("voo BuyTurn(false)");
            string price = Price.ToString();
            Shop.OpenPopupConfrim_2(price);
        }
        //string hehe = "{\"items\":{\"chip\":70062698,\"gold\":0,\"vippoint\":0,\"wheel\":15,\"iron_hammer\":0,\"silver_hammer\":0,\"gold_hammer\":1,\"exps\":{\"5\":17,\"10\":3,\"1\":10,\"6\":2,\"4\":2,\"2\":1}},\"msg\":\"B\u1ea1n \u0111\u00e3 mua th\u00e0nh c\u00f4ng 1 b\u00faa b\u1ea1c\",\"status\":1}";
        //BuyTurnComplete(hehe);
    }




    public void BuyGem()
    {
        Debug.Log("Mua gem ne------------------------");
        string price = Price.ToString();
        Shop.OpenPopupConfrim_GEM(price);
    }

    private void BuyTurnComplete(string s)
    {
        panelProcessingPayment.gameObject.SetActive(false);
        JSONNode jsonNode = JSONNode.Parse(s);
        Debug.Log(name + "====s====" + s);
        Debug.Log(name + "====node====" + jsonNode);

        string sms = jsonNode["msg"].Value;
        if (jsonNode["status"].AsInt == 1)
        {
            if (HammerType == "silver_hammer")
            {
                int Count_silver_hammer = jsonNode["items"]["silver_hammer"].AsInt;
                EvenDH.CapNhatBuaSauKhiMua(2, Count_silver_hammer);
            }
            else if (HammerType == "gold_hammer")
            {
                int Count_gold_hammer = jsonNode["items"]["gold_hammer"].AsInt;
                EvenDH.CapNhatBuaSauKhiMua(3, Count_gold_hammer);
            }
            AlertController.api.showAlert(sms, ClosePopupComplete);
        }
        else
        {
            AlertController.api.showAlert(sms, ClosePopupComplete);
        }
    }

    private void ClosePopupComplete()
    {
        // ClosePopupConfrim();
        ClosePopupCard();
    }

    public void UpdateChip(long t, int c)
    {
        /////
    }




}
