﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TweenAlpha : MonoBehaviour
{

    private bool inited = false;

    private Image imgTween;
    private float from, to;
    private float timeTween;
    private Color color;
    private Action callback;

    private float timeStart = 0;
    public void initTween(Image imgTween,Color color, float from, float to, float timeTween, Action callback = null)
    {
        this.imgTween = imgTween;
        this.from = from;
        this.to = to;
        this.color = color;
        this.timeTween = timeTween;
        this.callback = callback;
        timeStart = Time.realtimeSinceStartup;
        inited = true;
    }

    private void Update()
    {
        if (inited == true)
        {
            float process = (Time.realtimeSinceStartup - timeStart) / timeTween;
            color.a = Mathf.Lerp(from, to, process);
            imgTween.color = color;
            if (process >= 1)
            {
                if (callback != null)
                {
                    callback();
                }
                Destroy(this);
            }
        }
    }
}
