﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using BaseCallBack;
using System.Collections.Generic;

public class PopupAchivementManager : MonoBehaviour {

	[SerializeField]
	GameObject panel, panelNapTien, panelChip;
	[SerializeField]
	Toggle toggleNgay, toggleTuan, toggleTopThang, toggleTopChip;

    //[SerializeField]
    //GameObject panelTopTuan, panelTopEvent;

    [SerializeField]
    GameObject objItemTopRich, objItemTopNapTien;
	[SerializeField]
	Text txtToggleTopEvent;

    [SerializeField]
    Transform trsfRichParent, trsfNapTienParent;

	[SerializeField]
	Button btnExit;

	[SerializeField]
	GameObject Loading;

    [SerializeField]
    Text txtTienDaNap;
//	[SerializeField]
//	List<ItemTopChipView> lstTopChip;
//	[SerializeField]
//	List<ItemTopWinView> lstTopWin;

	string json = "";
	bool isFirst;
	
	onCallBack _backOnClick;
	public void Init(onCallBack _back )
	{
		isFirst = true;
        toggleTopChip.onValueChanged.AddListener(TogglePanelOnChange);
        toggleTuan.onValueChanged.AddListener (TogglePanelOnChange);
        toggleTopThang.onValueChanged.AddListener (TogglePanelOnChange);
        toggleNgay.onValueChanged.AddListener(TogglePanelOnChange);
        //toggleTopWinEvent.onValueChanged.AddListener (TogglePanelOnChange);

        //toggleTLNM.onValueChanged.AddListener (ToggleTLMNOnChange);
        //toggleXiTo.onValueChanged.AddListener (ToggleXiToOnChange);
        //toggleMB.onValueChanged.AddListener (ToggleMBOnChange);
        //togglePhom.onValueChanged.AddListener (TogglePhomOnChange);
        //      toggleTLDL.onValueChanged.AddListener(ToggleTLDLOnChange);  
        
        btnExit.onClick.AddListener (BtnExitOnClick);

		_backOnClick = _back;
        

	}

	public bool LOADING{
		set{ Loading.SetActive (value); }
	}

	void BtnExitOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
		_backOnClick ();
	}

	public void SetEnable(bool _enable)
	{
        
        if (_enable) {
            toggleNgay.isOn = true;
            Loading.SetActive (true);
            panelChip.SetActive(false);
            panelNapTien.SetActive(true);
        }
	}

	void ShowItemTop(int _index, string _name, long _chip)
	{
		ItemTopChipView itemView;
        //if (_index >= trsfRichParent.childCount) {
			GameObject obj = Instantiate (objItemTopRich) as GameObject;

			obj.transform.SetParent (trsfRichParent);

			obj.transform.SetSiblingIndex (_index);
			obj.transform.localScale = Vector3.one;
			obj.transform.position = Vector3.zero;

			itemView = obj.GetComponent<ItemTopChipView> ();

//			lstTopChip.Add (itemView);
		//} else {
		//	itemView = trsfRichParent.GetChild (_index).GetComponent<ItemTopChipView> ();
		//}
		itemView.Init ();
		itemView.Show (_index, _name, _chip);
	}
    void ShowItemTopNapTien(int _index, string _name, string _chip, int _count)
    {
        ItemTopWinView itemView;
       // if (_index >= trsfNapTienParent.childCount)
       // {
            GameObject obj = Instantiate(objItemTopNapTien) as GameObject;

            obj.transform.SetParent(trsfNapTienParent);

            obj.transform.SetSiblingIndex(_index);
            obj.transform.localScale = Vector3.one;
            obj.transform.position = Vector3.zero;

            itemView = obj.GetComponent<ItemTopWinView>();
            
        //}
       // else
        //{
        //    itemView = trsfNapTienParent.GetChild(_index).GetComponent<ItemTopWinView>();
      //  }       
        itemView.Init();
        itemView.Show(_index, _name, _chip,_count,0);
    }

    void ShowItemTopWinEvent(int _index, string _name, int _win, int _lose)
	{
        /*
		ItemTopWinEventView itemView;

		if (_index >= trsfEventParent.childCount) {
			GameObject obj = Instantiate (objItemTopEvent) as GameObject;

			obj.transform.SetParent (trsfEventParent);

			obj.transform.SetSiblingIndex (_index);
			obj.transform.localScale = Vector3.one;
			obj.transform.position = Vector3.zero;

			itemView = obj.GetComponent<ItemTopWinEventView> ();

			//			lstTopWin.Add (itemView);
		} else {
			itemView = trsfEventParent.GetChild (_index).GetComponent<ItemTopWinEventView> ();
		}
		itemView.Init ();
		itemView.Show(_index, _name, _win, _lose);
        */
	}

	void ClearitemTop(int _indexFrom)
	{
		for (int i = 0; i < trsfRichParent.childCount; i++) {
            //Debug.LogError("Loichilcount====" + i);
			GameObject.Destroy (trsfRichParent.GetChild (i).gameObject);
		}
	}
    void ClearitemTopNapTien()
    {
        for (int i = 0; i < trsfNapTienParent.childCount; i++)
        {
            GameObject.Destroy(trsfNapTienParent.GetChild(i).gameObject);
        }
    }
    void ClearitemTopWinEvent(int _indexFrom)
	{
        /*
		for (int i = _indexFrom; i < trsfEventParent.childCount; i++) {
			GameObject.Destroy (trsfEventParent.GetChild (i).gameObject);
		}
        */
	}

	#region Toggle OnChange

	void TogglePanelOnChange (bool _enable)
	{
		if (!_enable)
			return;
        //panelTopTuan.SetActive (toggleTuan.isOn);
        //panelTopEvent.SetActive (toggleTopChip.isOn);
        panelNapTien.SetActive(true);
        panelChip.SetActive(false);
        if (toggleNgay.isOn)
        {
            RequestTopNapTien("day");
        }
        else if (toggleTuan.isOn)
        {
            RequestTopNapTien("week");
        }
        else if (toggleTopThang.isOn)
        {
            RequestTopNapTien("month");
        }
        else if (toggleTopChip.isOn)
        {
            RequestTopChip();
            panelNapTien.SetActive(false);
            panelChip.SetActive(true);
        }
	}
    #endregion

    void RequestTopNapTien(string mode)
    {
        API.Instance.RequestTopNapTien(mode,RspTopNapTien);
        LOADING = true;
    }
    void RequestTopChip()
	{
		API.Instance.RequestTopChip (RspTopChip);
		LOADING = true;
	}

	void RequestTopWinEvent(){
		API.Instance.RequestTopWinEvent (RspTopWinEvent);
	}

	void RspTopWinEvent (string _json)
	{
		
		JSONNode node = JSONNode.Parse (_json);

		if (node==null || string.IsNullOrEmpty(node.ToString()))
			return;

		Debug.Log ("RSP ___ EVENT TOP:\n" + node.ToString ());

//		bool enable = true;

//		ToggleTopEvent.SetActive (enable);

		string sName = node["game"];

		txtToggleTopEvent.text = sName;

		JSONNode data = node ["data"];

		for (int i = 0; i < data.Count; i++) {
			string name = data [i] ["username"].Value;
			int win = data [i] ["win"].AsInt;
			int lose = data [i] ["lose"].AsInt;

			ShowItemTopWinEvent (i, name, win, lose);
		}
		ClearitemTopWinEvent (data.Count);
	}

	void RspTopChip(string _json)
	{
		JSONNode node = JSONNode.Parse (_json);

		JSONNode data = node ["data"];

		Debug.Log (data.Count + " - " + data.ToString ());
        ClearitemTop(data.Count);
        for (int i = 0; i < data.Count; i++)
        {
            Debug.LogError("iiii" + i);
            //for (int i = 0; i < 5; i++)        {
			string name = data [i] ["username"].Value;
			long chip = long.Parse (data [i] ["items"] ["chip"].Value);

			ShowItemTop (i, name, chip);
		}
		

		LOADING = false;
	}


    void RspTopNapTien(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);

        JSONNode data = node["data"];
        string curMoneyUser = data["mine"].Value;
        txtTienDaNap.text = curMoneyUser;
        Debug.Log(data.Count + " - " + data.ToString());
        JSONNode data2 = data["data"];
        ClearitemTopNapTien();
        for (int i = 0; i < data2.Count; i++)
        {
            //Debug.Log("iiiiiiiiii=====" + i);   
            string name = data2[i]["_id"].Value;
            string chip = data2[i]["total_amount"].Value;
            int count = int.Parse(data2[i]["count"].Value);
            ShowItemTopNapTien(i, name, chip, count);
        }
        

        LOADING = false;
    }

}

/*
 ========= CONTROLLER ========
 
 [SerializeField]
	PopupAchivementManager Achivement;
	
	Achivement.Init (BackAchivementOnClick);
	
public void BtnAchivementOnClick()
	{
		Achivement.SetEnable (true);
		SetEnableContent (false);
		_screen = ScreenLobby.Achivement;
	}
	void BackAchivementOnClick()
	{
		View.SetEnableContent (true);
		Achivement.SetEnable (false);
	}
	void SetEnableContent(bool _enable)
	{
		View.SetEnableContent (_enable);
	}

====== VIEW ======
[SerializeField]
	Button btnAchivement;

		btnAchivement.onClick.AddListener (BtnAchivementOnClick);


void BtnAchivementOnClick()
	{
		Controller.BtnAchivementOnClick ();
	}
	public void SetEnableContent(bool _enable)
	{
		panelUI.SetActive (_enable);
	}

 */
