﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;

public class PopupInboxManager : MonoBehaviour {

    private static PopupInboxManager instance;
    public static PopupInboxManager Instance
    {
        get
        {
            if (instance == null)
            {
                if (HomeViewV2.api != null)
                {
                    instance = HomeViewV2.api.Controller.Inbox;
                    return instance;
                }
                else 
                {
                    instance = HomeControllerV2.api.Inbox;
                    return instance;
                }
            }
            return instance;
        }
    }
    [SerializeField]
    GameObject panel;
    [SerializeField]
    Transform trsfPopup;
    [SerializeField]
    GameObject inboxList;
    [SerializeField]
    GameObject inboxDetail;
    [SerializeField]
    Button btnExit;
    onCallBack _exitOnClick;
    [SerializeField]
    Button btnBack;
    [SerializeField]
    GameObject objItemInbox;
    [SerializeField]
    Transform trsfItemInboxParent;
    [SerializeField]
    Text txtDetailFromName;
    [SerializeField]
    Text txtDetailTitle;
    [SerializeField]
    Text txtDetailDesc;
    [SerializeField]
    Text txtNoMessage;
    [SerializeField]
    Image ImgUnreadInbox;
    [SerializeField]
    Text TxtUnreadInbox;

    void Awake()
    {
        instance = this;
    }
    

    public void Init(onCallBack _back)
    {
        txtNoMessage.gameObject.SetActive(false);
        btnExit.onClick.AddListener(BtnExitOnClick);
        _exitOnClick = _back;
        btnBack.onClick.AddListener(BtnBackOnClick);
        RequestInbox();
    }

    void BtnExitOnClick()
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

        _exitOnClick();
    }

    public void addNotification()
    {
        MyInfo.UNREAD_INBOX = MyInfo.UNREAD_INBOX + 1;

        TxtUnreadInbox.text = MyInfo.UNREAD_INBOX + "";
        TxtUnreadInbox.gameObject.SetActive(true);
        ImgUnreadInbox.gameObject.SetActive(true);
    }

    void BtnBackOnClick()
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

        btnBack.gameObject.SetActive(false);
        inboxDetail.SetActive(false);
        inboxList.SetActive(true);
    }

    public void BtnSelectOnClick(Inbox inbox)
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

        txtDetailTitle.text = inbox.Title;
        txtDetailFromName.text = inbox.Author;
        txtDetailDesc.text = inbox.Content;
        inboxList.SetActive(false);
        inboxDetail.SetActive(true);
        btnBack.gameObject.SetActive(true);
    }

    public void BtnDeleteOnClick(string id)
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

        print("inboxes count 1: " + MyInfo.INBOXES.Count);

        MyInfo.INBOXES = MyInfo.INBOXES.Where(x => !x.Id.Equals(id)).ToList();

        print("inboxes count 2: " + MyInfo.INBOXES.Count);
        RequestDelete(id);
    }

    void ShowItemInbox(int index, Inbox inbox)
    {
        ItemInboxView itemView;
        GameObject obj = Instantiate(objItemInbox) as GameObject;
        obj.transform.SetParent(trsfItemInboxParent);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<ItemInboxView>();
        itemView.Init(inbox);
        itemView.Show(index);
    }

    public void RequestInbox()
    {
        Debug.LogError("RequestInbox nekkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
        API.Instance.RequestInbox(ResponseInbox);
    }

    void RequestDelete(string id)
    {
        print("RequestDelete " + id);
        API.Instance.RequestDeleteInbox(id, ResponseDeleteInbox);
    }

    void ResponseInbox(string json)
    {
        print("ResponseInbox " + json.ToString());
        JSONNode data = JSONNode.Parse(json);

        MyInfo.INBOXES = new List<Inbox>();

        for (int i = 0; i < data.Count; i++)
        {
            Inbox inbox = new Inbox();
            inbox.Id = data[i]["id"].Value;
            inbox.Title = data[i]["title"].Value;
            inbox.Content = data[i]["desc"].Value;
            inbox.Author = data[i]["from_name"].Value;
            inbox.CreatedDate = DateTime.Parse(data[i]["created_date"].Value);
            MyInfo.INBOXES.Add(inbox);
        }

        UpdateInboxes();
    }

    void ResponseDeleteInbox(string json)
    {
        UpdateInboxes();
    }

    public void Show()
    {
        MyInfo.UNREAD_INBOX = 0;
        TxtUnreadInbox.gameObject.SetActive(false);
        ImgUnreadInbox.gameObject.SetActive(false);
        inboxDetail.SetActive(false);
        inboxList.SetActive(true);
        btnBack.gameObject.SetActive(false);
        panel.SetActive(true);
        GameHelper.DoOpen(trsfPopup, OnShow);
    }

    public void Hide()
    {
        GameHelper.DoClose(trsfPopup, OnHide);
    }

    void OnShow()
    {

    }

    void OnHide()
    {
        panel.SetActive(false);
    }

    private void UpdateInboxes()
    {
        for (int i = 0; i < trsfItemInboxParent.childCount; i++)
            Destroy(trsfItemInboxParent.GetChild(i).gameObject);

        for (int i = 0; i < MyInfo.INBOXES.Count; i++)
            ShowItemInbox(i, MyInfo.INBOXES[i]);

        if (MyInfo.INBOXES.Count > 0)
            txtNoMessage.gameObject.SetActive(false);
        else
            txtNoMessage.gameObject.SetActive(true);
    }
}

public class Inbox {
    public string Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public string Author { get; set; }
    public DateTime CreatedDate { get; set; }
}