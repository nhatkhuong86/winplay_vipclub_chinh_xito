﻿using BaseCallBack;
using GoogleMobileAds.Api;
using SimpleJSON;
using UnityEngine;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif
public class VideoAdsManager : MonoBehaviour
{
    public static VideoAdsManager Instance = null;
    private string vungleRewardId = "";
    private string adsSecretkey = "";
    private bool isCallService = false;
    bool adInited = false;
    [SerializeField]
    AdmobAdsManager AdmobAds;
    void Start()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
        AdmobAds.Init(CompleteVideoAdmob);
    }
    

    private void OnEnable()
    {
        
    }
    

    onCallBackInt callbackCompleteVideo;
    public void ShowAds(onCallBackInt callBack = null)
    {
        callbackCompleteVideo = callBack;
        API.Instance.RequestGlobalAds(RspRequestAds);
    }

    void RspRequestAds(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" ========================RspRequestAds=============== " + _json);
        if (node["status"].AsInt == 1)
        {
            adsSecretkey = node[ParamKey.ADS_SECRET_KEY].Value;
#if UNITY_IOS || UNITY_ANDROID
            ShowUnityVideoRewardAd();
#endif
        }
    }
    private void callReceiveGiftWatched()
    {
        if (adsSecretkey != "")
        {
            API.Instance.RewardGlobalAds(adsSecretkey, RspRewardCallback);
        }
    }
#if UNITY_IOS || UNITY_ANDROID
    public string placementId = "rewardedVideo";
    void ShowUnityVideoRewardAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;
        Advertisement.Show(placementId, options);
    }



    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            callReceiveGiftWatched();
        }
        else if (result == ShowResult.Skipped)
        {
            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
        }
        else if (result == ShowResult.Failed)
        {
            if (AdmobAds.rewardedAd.IsLoaded())
            {
                AdmobAds.rewardedAd.Show();
            }
            else
            {
                //AlertController.api.showAlert("Hiện không có video nào khả dụng, vui lòng quay lại sau");
                callReceiveGiftWatched();
            }
        }
    }
    void CompleteVideoAdmob(int result)
    {
        if (result == 1)
        {
            callReceiveGiftWatched();
        }else
        {
            AlertController.api.showAlert("Hiện không có video nào khả dụng, vui lòng quay lại sau ít phút");
        }
        
    }
#endif
    void RspRewardCallback(string _json)
    {
        Debug.Log("=============RspRewardCallback==========" + _json);
        JSONNode node = JSONNode.Parse(_json);
        if (node["status"].AsInt == 1)
        {
        
            double mychip = double.Parse(node[ParamKey.CHIP].Value);
            int chipReward = (int)(mychip - MyInfo.CHIP);
            //Debug.LogError("=============********************************==========");
            //Debug.LogError("=============node[ParamKey.CHIP].Value==========" + node[ParamKey.CHIP].Value);
            //Debug.LogError("=============MyInfo.CHIP==========" + MyInfo.CHIP);
            //Debug.LogError("=============chipReward==========" + chipReward);
            //Debug.LogError("=============********************************==========");

            if (HomeViewV2.api != null)
            {
                AlertController.api.showAlert("Bạn nhận được " + chipReward + " chip");
                callbackCompleteVideo(chipReward);
                //MyInfo.CHIP = long.Parse(mychip.ToString());//ở trang home thì khong set vi animation da~ cong chip roi 
            }
            else
            {
                MyInfo.CHIP = long.Parse(mychip.ToString());
                callbackCompleteVideo(chipReward);
            }
        }
        else if (node["status"].AsInt != 1)
        {
            AlertController.api.showAlert("Có lỗi khi xử lý thưởng xem video, vui lòng liên hệ admin");
        }

    }
    //void initializeEventHandlers()
    //{
    //    Vungle.onAdStartedEvent += (placementID) =>
    //    {
    //        Debug.Log("Ad " + placementID + " is starting!  Pause your game  animation or sound here.");
    //    };
    //    Vungle.onAdFinishedEvent += (placementID, args) =>
    //    {
    //        Debug.Log("Ad finished - placementID " + placementID + ", was call to action clicked:" + args.WasCallToActionClicked + ", is completed view:"
    //            + args.IsCompletedView);
    //        if (args.IsCompletedView == true)
    //        {
    //            callReceiveGiftWatched();
    //        }
    //        else
    //        {
    //            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
    //        }
    //    };


    //    Vungle.adPlayableEvent += (placementID, adPlayable) =>
    //    {
    //        Debug.Log("Ad's playable state has been changed! placementID " + placementID + ". Now: " + adPlayable);
    //        placements[placementID] = adPlayable;
    //    };

    //    Vungle.onInitializeEvent += () =>
    //    {
    //        adInited = true;
    //    };
    //}
}
