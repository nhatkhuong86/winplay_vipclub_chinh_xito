﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using BaseCallBack;
using DG.Tweening;

namespace ViewMB {
		
	public class MBRowView : MonoBehaviour {

		[SerializeField]
		private List<GameObject> lstPanelAll;

        [SerializeField]
        private List<Text> lstTextPhaseResultP0, lstTextPhaseResultP1, lstTextPhaseResultP2, lstTextPhaseResultP3;

        private Dictionary<int, List<Text>> dicPlayerResult;
		[SerializeField]
		private List<Text> 

			lstTxtResultRow,
			lstTxtResultRowChip;
			
		[SerializeField]
				List<Image> lstImgValueRowsP0,
				lstImgValueRowsP1,
				lstImgValueRowsP2,
				lstImgValueRowsP3,

				lstImgWin, lstImgFail;


				private List<List<Image>> lstValueRowsAll;

		onCallBack onFinish;

		private const float TIME_DELAY = 4;

		long CASH_IN;
		public void Init()
        {
            lstValueRowsAll = new List<List<Image>>();


            lstValueRowsAll.Add(lstImgValueRowsP0);
            lstValueRowsAll.Add(lstImgValueRowsP1);
            lstValueRowsAll.Add(lstImgValueRowsP2);
            lstValueRowsAll.Add(lstImgValueRowsP3);

            dicPlayerResult = new Dictionary<int, List<Text>>();
            dicPlayerResult[0] = lstTextPhaseResultP0;
            dicPlayerResult[1] = lstTextPhaseResultP1;
            dicPlayerResult[2] = lstTextPhaseResultP2;
            dicPlayerResult[3] = lstTextPhaseResultP3;

        }

        public void deactiveAllTextResult()
        {
            foreach (KeyValuePair<int, List<Text>> item in dicPlayerResult)
            {
                for (int k = 0; k < item.Value.Count; k++)
                {
                    item.Value[k].text = "0";
                    item.Value[k].transform.parent.gameObject.SetActive(false);
                }
            }

        }
		public void InitBet(long _bet)
		{
			CASH_IN = _bet;
		}
		bool _enable = false;

		public void Enable(int _position, bool _enable)
		{
			lstPanelAll [_position].SetActive (_enable);
		}

		public void ShowValueRow(int _position, int _row, MBType _type)
		{
            Debug.LogWarning("positon " + _position + "     row: " + _row);
            dicPlayerResult[_position][_row - 1].transform.parent.DOScale(Vector2.one * 1.5f, 0.2f);

            lstValueRowsAll[_position][_row - 1].gameObject.SetActive(true);

            lstValueRowsAll[_position][_row - 1].sprite = MBDataHelper.instance.GetSprRow(_type);
        }
		public void ShowValueRows(int _position, MBType _row1, MBType _row2, MBType _row3)
		{
			lstValueRowsAll [_position] [0].gameObject.SetActive (true);
			lstValueRowsAll [_position] [1].gameObject.SetActive (true);
			lstValueRowsAll [_position] [2].gameObject.SetActive (true);

            lstValueRowsAll[_position][0].sprite = MBDataHelper.instance.GetSprRow(_row1);
            lstValueRowsAll[_position][1].sprite = MBDataHelper.instance.GetSprRow(_row2);
            lstValueRowsAll[_position][2].sprite = MBDataHelper.instance.GetSprRow(_row3);
        }
		public void HideValueRow(int _position, int _row)
		{
			lstValueRowsAll [_position] [_row - 1].gameObject.SetActive (false);
		}
		public void HideValueRows(int _position)
		{
			lstValueRowsAll [_position] [0].gameObject.SetActive (false);
			lstValueRowsAll [_position] [1].gameObject.SetActive (false);
			lstValueRowsAll [_position] [2].gameObject.SetActive (false);
		}

		public void SetEnableValueFail(int _position, bool _enable)
		{
			lstImgFail [_position].gameObject.SetActive (_enable);
		}


		public void ShowValueWin(int _position, MBTypeImmediate _type, float _timeDelay = TIME_DELAY, onCallBack _onFinish = null)
		{
			lstImgWin [_position].gameObject.SetActive (true);
			lstImgWin [_position].sprite = MBDataHelper.instance.GetSprWin (_type);
		}
		IEnumerator ShowValueWinThread(int _position, MBTypeImmediate _type, float _timeDelay = TIME_DELAY, onCallBack _onFinish = null)
		{
					Debug.Log ("Pos WIN: " + _position + " --- " + _type);

					lstImgWin [_position].gameObject.SetActive (true);
						lstImgWin [_position].sprite = MBDataHelper.instance.GetSprWin (_type);

			onFinish = _onFinish;
			yield return new WaitForSeconds (_timeDelay);
			onFinish ();
		}
		public void HideValueWin(int _position)
		{
			lstImgWin [_position].gameObject.SetActive (false);
		}


        public void showAllResultPerRow(int _position)
        {
            for (int i = 0; i < dicPlayerResult[_position].Count; i++)
            {
                dicPlayerResult[_position][i].transform.parent.gameObject.SetActive(true);//show thong tin thang thua truoc do
            }
           
        }
		/// <summary>
		/// Shows the result row.
		/// </summarầy>
		public void ShowResultRow(int _position, int _resultRow, long _resultChip, int _phase = 0)
		{
			string phaseName = "";
			switch (_phase) {
			case 1:
				phaseName = "Chi đầu: ";
				break;
			case 2:
				phaseName = "Chi giữa: ";
				break;
			case 3:
				phaseName = "Chi cuối: ";
				break;
			case 4:
				phaseName = "Chi Át: ";
				break;
			case 5:
				phaseName = "Kết quả: ";
				break;
			}
            
            Color colorSet = Color.yellow;

            if (_resultChip < 0)
            {
               
                lstTxtResultRowChip[_position].color = Color.red;
            }
            else
            {
                
                lstTxtResultRowChip[_position].color = Color.yellow;
            }



            if (_resultChip < 0)
            {
                colorSet = Color.red;
                lstTxtResultRowChip[_position].text = Utilities.GetStringMoneyByLong(_resultChip);
            }
            else
            {
                colorSet = Color.yellow;
                lstTxtResultRowChip[_position].text = "+" + Utilities.GetStringMoneyByLong(_resultChip);
            }
            
            if (_phase <= 3)//3 chi cứng
            {
                Color colorResult = Ulti.ConvertHexStringToColor("E3E3E3FF");
                dicPlayerResult[_position][_phase - 1].transform.parent.gameObject.SetActive(true);
                if (_phase - 2 >= 0)
                {
                    dicPlayerResult[_position][_phase - 2].transform.parent.gameObject.SetActive(false);//hide chi truoc do
                    dicPlayerResult[_position][_phase - 2].transform.parent.DOScale(Vector2.one, 0.1f);
                }
                dicPlayerResult[_position][_phase - 1].text = Utilities.GetStringMoneyByLong(_resultChip, true);
                if (_resultChip > 0)
                {
                    colorResult = Ulti.ConvertHexStringToColor("FFFF00FF");
                }
                dicPlayerResult[_position][_phase - 1].color = colorResult;
            }
            else if (_phase == 4)//so chi at
            {
                lstTxtResultRow[_position].text = phaseName;
                dicPlayerResult[_position][_phase - 2].transform.parent.DOScale(Vector2.one, 0.1f);
                dicPlayerResult[_position][_phase - 2].transform.parent.gameObject.SetActive(false);
                lstTxtResultRowChip[_position].transform.parent.gameObject.SetActive(true);
            }
            else//ket qua
            {
                lstTxtResultRow[_position].text = phaseName;
            }
            lstTxtResultRowChip[_position].color = colorSet;


        }
		public void HideResultRow(int _position)
		{
			lstTxtResultRow [_position].transform.parent.gameObject.SetActive (false);
		}

	}
}