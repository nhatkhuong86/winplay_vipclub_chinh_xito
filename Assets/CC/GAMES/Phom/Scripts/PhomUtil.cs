﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using UnityEngine.UI;

/// <summary>
/// 
/// Terminologies in RummyVN Game
/// 
/// Meld: Can either be a set or a run
/// Run: Consists of at least three consecutive cards of the same suit
/// Set: Consists of at least three cards of the same rank
/// Wait: Cards wait to become run or set
/// Run Wait: Cards wait to become a run
/// Set Wait: Cards wait to become a set
/// Trash: Cards that are not in any runs, sets or waits
/// Combination: A way of sorting cards in hand
/// 
/// References:
/// https://en.wikipedia.org/wiki/Rummy
/// https://vi.wikipedia.org/wiki/Phỏm
/// 
/// </summary>
public class PhomUtil : MonoBehaviour {

    #region Public Main Methods

    public static int GetMeldOrder(int cardId, Combination combination)
    {
        int meldOrder = 0;

        if (combination.ChosenMeld != null && combination.ChosenMeld.Count > 0)
        {
            foreach (PhomCard card in combination.ChosenMeld)
            {
                if (cardId == card.Id)
                    return meldOrder;
            }
            meldOrder++;
        }

        foreach (List<PhomCard> set in combination.Sets)
        {
            foreach (PhomCard card in set)
            {
                if (cardId == card.Id)
                    return meldOrder;
            }
            meldOrder++;
        }

        foreach (List<PhomCard> cards in combination.Runs)
        {
            foreach (PhomCard card in cards)
            {
                if (cardId == card.Id)
                    return meldOrder;
            }
            meldOrder++;
        }

        return PhomConst.INVALID_INDEX;
    }

    public static int GetEnemySlotIndexByPosition(int playerPos, int enemyPos)
    {
        /*
        switch (playerPos)
        {
            case 0:
                switch (enemyPos)
                {
                    case 1: return 2;
                    case 2: return 1;
                    case 3: return 0;
                }
                break;
            case 1:
                switch (enemyPos)
                {
                    case 0: return 0;
                    case 2: return 2;
                    case 3: return 1;
                }
                break;
            case 2:
                switch (enemyPos)
                {
                    case 0: return 1;
                    case 1: return 0;
                    case 3: return 2;
                }
                break;
            case 3:
                switch (enemyPos)
                {
                    case 0: return 2;
                    case 1: return 1;
                    case 2: return 0;
                }
                break;
        }
        */

        switch (playerPos)
        {
            case 0:
                switch (enemyPos)
                {
                    case 1: return 0;
                    case 2: return 1;
                    case 3: return 2;
                }
                break;
            case 1:
                switch (enemyPos)
                {
                    case 0: return 2;
                    case 2: return 0;
                    case 3: return 1;
                }
                break;
            case 2:
                switch (enemyPos)
                {
                    case 0: return 1;
                    case 1: return 2;
                    case 3: return 0;
                }
                break;
            case 3:
                switch (enemyPos)
                {
                    case 0: return 0;
                    case 1: return 1;
                    case 2: return 2;
                }
                break;
        }

        return PhomConst.INVALID_INDEX;
    }

    public static PhomEnemySlot GetEnemySlotByPosition(List<PhomEnemySlot> enemies, int playerPos, int enemyPos)
    {
        return enemies[GetEnemySlotIndexByPosition(playerPos, enemyPos)];
    }

    public static void ResetCardsState(List<PhomCard> cards)
    {
        foreach (PhomCard card in cards)
        {
            if (card == null)
                continue;
            card.transform.GetChild(PhomConst.CARD_MASK_INDEX).gameObject.GetComponent<Image>().color = PhomColor.TransparentColor;
            card.MeldMarkImg.gameObject.SetActive(false);
            card.transform.localScale = new Vector3(0.7f, 0.7f, 1);
        }
    }

    public static bool isSomeoneRummy(List<PhomSlot> slots)
    {
        foreach (PhomSlot slot in slots)
            if (isRummy(slot))
                return true;
        return false;
    }

    public static bool isRummy(PhomSlot slot)
    {
        if (slot.EndGameStatusInfo.Equals(PhomConst.STATUS_WIN_BY_9_MELD_CARDS)
            || slot.EndGameStatusInfo.Equals(PhomConst.STATUS_WIN_BY_9_TRASH_CARDS)
            || slot.EndGameStatusInfo.Equals(PhomConst.STATUS_WIN_BY_10_MELD_CARDS)
            || slot.EndGameStatusInfo.Equals(PhomConst.STATUS_WIN_BY_10_TRASH_CARDS))
            return true;
        return false;
    }

    public static bool isRummyOrLoseForAll(PhomSlot slot)
    {
        if (isRummy(slot) || slot.EndGameStatusInfo.Equals(PhomConst.STATUS_LOSE_FOR_ALL))
            return true;
        return false;
    }

    public static List<PhomCard> GetUpperLaidDownCards(Combination laidDownCombination)
    {
        foreach (List<PhomCard> cards in laidDownCombination.Sets)
            return cards;
        foreach (List<PhomCard> cards in laidDownCombination.Runs)
            return cards;
        return null;
    }

    public static List<PhomCard> GetLowerLaidDownCards(Combination laidDownCombination)
    {
        if (GetUpperLaidDownCards(laidDownCombination) == null)
            return null;

        bool isUpperLaidDownCards = true;

        foreach (List<PhomCard> cards in laidDownCombination.Sets)
        {
            if (!isUpperLaidDownCards)
                return cards;
            isUpperLaidDownCards = false;
        }

        foreach (List<PhomCard> cards in laidDownCombination.Runs)
        {
            if (!isUpperLaidDownCards)
                return cards;
            isUpperLaidDownCards = false;
        }

        return null;
    }

    #endregion

    #region Animate

    public static void SortCardsOrderInZone(List<PhomCard> cards, GameObject zone)
    {
        foreach (PhomCard card in cards)
            foreach (Transform transform in zone.transform)
                if (transform.GetComponent<PhomCard>().Id == card.Id)
                    transform.SetAsLastSibling();
    }

    #endregion

    #region Check Cards Methods

    public static bool IsMeld(List<PhomCard> cards)
    {
        return IsRun(cards) || IsSet(cards);
    }

    public static bool IsRun(List<PhomCard> cards)
	{
		if (cards.Count < 3)
			return false;
		SortById (cards);
		PhomCard temp = cards [0];
		for(int i = 1; i < cards.Count; i++)
		{
			if ((cards [i].Value == (temp.Value + 1)) && (cards[i].Type.Equals(temp.Type)))
				temp = cards [i];
			else
				return false;
		}
		return true;
	}

    public static bool IsSet(List<PhomCard> cards)
	{
		return IsThreeOfAKind (cards) || IsFourOfAKind (cards);
	}

    public static bool IsThreeOfAKind(List<PhomCard> cards)
	{
		if (cards.Count != 3)
			return false;
		if (cards [0].Value == cards [1].Value && cards [1].Value == cards [2].Value)
			return true;
		return false;
	}

    public static bool IsFourOfAKind(List<PhomCard> cards)
	{
		if (cards.Count != 4)
			return false;
		if (cards [0].Value == cards [1].Value
			&& cards [1].Value == cards [2].Value
			&& cards [2].Value == cards [3].Value)
			return true;
		return false;
	}

    public static bool IsMeldInCombination(List<PhomCard> meld, Combination combination)
    {
        if (IsSameCards(meld, combination.ChosenMeld))
            return true;

        foreach (List<PhomCard> cards in combination.Runs)
            if (IsSameCards(meld, cards))
                return true;

        foreach (List<PhomCard> cards in combination.Sets)
            if (IsSameCards(meld, cards))
                return true;

        return false;
    }

    private static bool IsSameCards(List<PhomCard> cards1, List<PhomCard> cards2)
    {
        if (cards1.Count != cards2.Count)
            return false;

        List<PhomCard> clonedCards1 = Clone(cards1);
        List<PhomCard> clonedCards2 = Clone(cards2);
        SortById(clonedCards1);
        SortById(clonedCards2);

        for (int i = 0; i < clonedCards1.Count; i++)
            if (clonedCards1[i].Id != clonedCards2[i].Id)
                return false;

        return true;
    }

	#endregion

	#region List Util Methods

    public static PhomCard getCardById(int id)
    {
        List<PhomCard> allCards = PhomRoomController.Instance.Deck.Cards;
        foreach (PhomCard card in allCards)
            if (card.Id == id)
                return card;
        return null;
    }

    #endregion

	#region Sort Util Methods

	private static void SortById(List<PhomCard> cards)
	{
		cards.Sort (delegate (PhomCard x, PhomCard y)
		{
			return x.Id.CompareTo(y.Id);
		});
	}

    #endregion

    #region Clone Util Methods

    public static PhomSlot Clone(PhomSlot slot) {
        PhomSlot clonedSlot;

        if (slot is PhomPlayerSlot)
            clonedSlot = new PhomPlayerSlot();
        else
            clonedSlot = new PhomEnemySlot();

        clonedSlot.IsWin = slot.IsWin;
        clonedSlot.ChangedMoneyInfo = slot.ChangedMoneyInfo;
        clonedSlot.EndGameStatusInfo = slot.EndGameStatusInfo;
        clonedSlot.Combination = Clone(slot.Combination);

        return clonedSlot;
    }

	public static List<PhomCard> Clone (List<PhomCard> cards)
	{
		List<PhomCard> clonedCards = new List<PhomCard> ();
		foreach (PhomCard card in cards)
			clonedCards.Add (card);
		return clonedCards;
	}

	private static List<List<PhomCard>> Clone (List<List<PhomCard>> cardLists)
	{
		List<List<PhomCard>> clonedLists = new List<List<PhomCard>> ();
		foreach (List<PhomCard> cards in cardLists)
			clonedLists.Add(Clone(cards));
		return clonedLists;
	}

	public static Combination Clone (Combination combination)
	{
		Combination clonedCombination = new Combination ();
		clonedCombination.All = Clone (combination.All);
		clonedCombination.Runs = Clone (combination.Runs);
		clonedCombination.Sets = Clone (combination.Sets);
		clonedCombination.Waits = Clone (combination.Waits);
		clonedCombination.Trashs = Clone (combination.Trashs);
		return clonedCombination;
	}

    #endregion

    #region Convert Util Methods

    public static string ConvertCardCodesToCardIds(string strCodes)
    {
        StringBuilder builder = new StringBuilder();
        List<string> codes = strCodes.Split(DelimiterKey.comma).ToList();
        foreach (string strCode in codes)
        {
            string id = ConvertCardCodeToCardId(strCode);
            builder.Append(id);
            builder.Append(DelimiterKey.underscore);
        }
        if (builder.Length >= 1)
            builder.Remove(builder.Length - 1, 1);
        return builder.ToString();
    }

    public static string ConvertCardCodeToCardId(string strCode)
    {
        if (string.IsNullOrEmpty(strCode))
            return string.Empty;
        char cType = strCode[strCode.Length - 1];
        int type = (int)Char.GetNumericValue(cType);
        string strValue = strCode.Remove(strCode.Length - 1);
        int value = int.Parse(strValue);
        int id = 13 * type + value - 1;
        return id.ToString();
    }

    public static List<PhomSlot> ConvertStringToSlots(string strSlots) {
        List<PhomSlot> slots = new List<PhomSlot>();

        if (string.IsNullOrEmpty(strSlots))
            return slots;

        List<string> strSlotParts = strSlots.Split(DelimiterKey.dollarPack).ToList();

        foreach (string strSlot in strSlotParts)
        {
            if (string.IsNullOrEmpty(strSlot))
                continue;

            List<string> parts = strSlot.Split(DelimiterKey.verticalBarPack).ToList();
            int sfsId = int.Parse(parts[PhomConst.SFS_ID_INDEX]);
            bool isWin = bool.Parse(parts[PhomConst.IS_WIN_INDEX]);
            long gold = long.Parse(parts[PhomConst.GOLD_INDEX]);
            long changedMoney = long.Parse(parts[PhomConst.CHANGED_MONEY_INDEX]);
            string endGameStatus = parts[PhomConst.END_GAME_STATUS];
            string strCombination = parts[PhomConst.COMBINATION];
            string strDiscardedCards = parts[PhomConst.DISCARDED_CARDS_INDEX];
            string strEatenCards = parts[PhomConst.EATEN_CARDS_INDEX];
            string strUpperLaidDownCards = parts[PhomConst.UPPER_LAID_DOWN_CARDS_INDEX];
            string strLowerLaidDownCards = parts[PhomConst.LOWER_LAID_DOWN_CARDS_INDEX];
            bool isWatcher = bool.Parse(parts[PhomConst.IS_WATCHER]);
            int vipPoint = int.Parse(parts[PhomConst.VIP_POINT]);

            PhomSlot slot = null;

            if (sfsId == MyInfo.SFS_ID)
            {
                slot = new PhomPlayerSlot();
                slot.Combination = ToCombination(strCombination);
            }
            else
            {
                slot = new PhomEnemySlot();
                slot.Combination = ToCombination(strCombination);
            }

            slot.SfsId = sfsId;
            slot.IsWin = isWin;
            slot.GoldInfo = gold;
            slot.ChangedMoneyInfo = changedMoney;
            slot.EndGameStatusInfo = endGameStatus;
            slot.DiscardedCards = ToCards(strDiscardedCards);
            slot.EatenCards = ToCards(strEatenCards);
            slot.UpperLaidDownCards = ToCards(strUpperLaidDownCards);
            slot.LowerLaidDownCards = ToCards(strLowerLaidDownCards);
            slot.IsWatcher = isWatcher;
            slot.VipPoint = vipPoint;

            slots.Add(slot);
        }

        return slots;
    }

    public static List<SentGroup> ConvertStringToSentGroups(string strSentGroups)
    {
        if (string.IsNullOrEmpty(strSentGroups))
            return new List<SentGroup>();

        List<SentGroup> sentGroups = new List<SentGroup>();
        List<string> strSentGroupParts = strSentGroups.Split(DelimiterKey.colon).ToList();

        foreach (string strSentGroup in strSentGroupParts)
        {
            SentGroup sentGroup = new SentGroup();
            List<string> parts = strSentGroup.Split(DelimiterKey.semicolon).ToList();
            
            sentGroup.TakenSlotId = int.Parse(parts[PhomConst.TAKEN_SLOT_ID_INDEX]);
            sentGroup.UpperSentCards = ToCards(parts[PhomConst.UPPER_SENT_CARDS_INDEX]);
            sentGroup.LowerSentCards = ToCards(parts[PhomConst.LOWER_SENT_CARDS_INDEX]);
            sentGroup.NewUpperLaidDownCards = ToCards(parts[PhomConst.NEW_UPPER_LAID_DOWN_CARDS_INDEX]);
            sentGroup.NewLowerLaidDownCards = ToCards(parts[PhomConst.NEW_LOWER_LAID_DOWN_CARDS_INDEX]);
            sentGroups.Add(sentGroup);
        }
        return sentGroups;
    }

    public static Combination ToCombination(string strCombination)
    {
        if (string.IsNullOrEmpty(strCombination))
            return new Combination();

        Combination combination = new Combination();
        List<string> parts = strCombination.Split(DelimiterKey.hash).ToList();
        combination.All = ToCards(parts[PhomConst.ALL_INDEX]);
        combination.ChosenMeld = ToCards(parts[PhomConst.CHOSEN_MELD_INDEX]);
        combination.Runs = ConvertStringToCardLists(parts[PhomConst.RUNS_INDEX]);
        combination.Sets = ConvertStringToCardLists(parts[PhomConst.SETS_INDEX]);
        combination.Waits = ToCards(parts[PhomConst.WAITS_INDEX]);
        combination.Trashs = ToCards(parts[PhomConst.TRASHS_INDEX]);
        return combination;
    }

    public static List<PhomCard> ToCards(string strCards)
    {
        List<PhomCard> cards = new List<PhomCard>();
        if (string.IsNullOrEmpty(strCards))
            return cards;

        List<string> strCardIds = strCards.Split(DelimiterKey.underscore).ToList();
        foreach (string strCardId in strCardIds)
        {
            PhomCard card = getCardById(int.Parse(strCardId));
            cards.Add(card);
        }
        return cards;
    }

    public static List<List<PhomCard>> ConvertStringToCardLists(string str)
    {
        List<List<PhomCard>> cardLists = new List<List<PhomCard>>();
        if (string.IsNullOrEmpty(str))
            return cardLists;
        
        List<string> strCardLists = str.Split(DelimiterKey.comma).ToList();
        foreach (string strCardIds in strCardLists)
        {
            List<PhomCard> cards = ToCards(strCardIds);
            cardLists.Add(cards);
        }
        return cardLists;
    }

    public static List<int> ConvertCardsToIds(List<PhomCard> cards)
    {
        List<int> ids = new List<int>();
        foreach (PhomCard card in cards)
            ids.Add(card.Id);
        return ids;
    }

	#endregion

	#region Print Util Methods

	public static void Print(PhomCard card)
	{
		if (card == null)
		{
			print ("Null card!");
			return;
		}
		print ("Id: " + card.Id + "; Value: " + card.Value + "; Type: " + card.Type);
	}

	public static void PrintList(List<PhomCard> cards)
	{
		if (cards == null)
		{
			print ("Null cards!");
			return;
		}
		foreach (PhomCard card in cards)
			Print (card);
	}

	public static void PrintLists(List<List<PhomCard>> cardLists)
	{
		if (cardLists == null)
		{
			print ("Null card lists!");
			return;
		}
		if (cardLists.Count == 0)
		{
			print ("Card lists count = 0");
			return;
		}
		for (int i = 0; i < cardLists.Count; i++)
		{
			print ("Card List " + i);
			PrintList (cardLists [i]);
		}
	}

	public static void Print(Combination combination)
	{
        /*print("All:");
        PrintList(combination.All);
		print ("Runs: ");
        PrintLists(combination.Runs);
		print ("Sets: ");
        PrintLists(combination.Sets);
		print ("Run Waits");
		PrintLists (combination.RunWaits);
		print ("Set Waits");
		PrintLists (combination.SetWaits);*/
        print("Sorted Cards");
        PrintList(combination.GetSortedCards());
    }

    #endregion
}