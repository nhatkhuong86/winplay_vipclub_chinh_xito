﻿using UnityEngine;
using System.Collections;

namespace ConstXT {
	
	public class XTCommandKey {

		public const string START_GAME = "8";
		public const string PICK_CARD = "111";
		public const string RAISE = "112";
		public const string CHECK = "113";
		public const string FOLD = "114";
		public const string DEAL_A_CARD = "115";
		public const string DEAL_FINAL_CARD = "116";
		public const string FINISH_GAME = "15";
	}
}