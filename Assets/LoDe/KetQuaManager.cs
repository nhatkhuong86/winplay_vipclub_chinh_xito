﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class ResultLotteryManager4 : MonoBehaviour {

    private SFS sfs;
    private static ResultLotteryManager instance;

    public static ResultLotteryManager Instance { get { return instance; } }

    [SerializeField]
    GameObject PanelKetQua;
    [SerializeField]
    Button BtnBack;

 //   [SerializeField]
 //   Button BtnExit;

    [SerializeField]
    Button BtnDac;

    [SerializeField]
    Button BtnNhi;

    [SerializeField]
    Button BtnBa;

    [SerializeField]
    Button BtnTu;

    [SerializeField]
    Button BtnSau;

    [SerializeField]
    Button BtnThangThua;

    [SerializeField]
    Button BtnExitThangThua;

    [SerializeField]
    Button BtnLichSu;

  //  [SerializeField]
   // Text TxtThangThua;



    [SerializeField]
    GameObject List_KetQua;

    [SerializeField]
    GameObject List_ThangThua;

    [SerializeField]
    Transform Parent_KetQua;

    [SerializeField]
    Transform Parent_ThangThua;


    [SerializeField]
    GameObject ThangThuaModule;


    [SerializeField]
    GameObject DacBietModule;

    [SerializeField]
    GameObject GiaiNhatModule;

    [SerializeField]
    GameObject GiaiNhiModule;

    [SerializeField]
    GameObject GiaiBaModule;

    [SerializeField]
    GameObject GiaiTuModule;

    [SerializeField]
    GameObject GiaiSaModule;

   // [SerializeField]
   // Image ImgFace;

    [SerializeField]
    GameObject PanelThangThua;

    [SerializeField]
    GameObject PanelThongTinKetQua;

    [SerializeField]
    PopupLotteryManager LotteryManager;
    [SerializeField]
    GameObject Vien;
    [SerializeField]
    GameObject PanelThongBaoMatKetNoiMayChu;
    [SerializeField]
    Button BtnExitPanelThongBaoMatKetNoiMayChu;


    [SerializeField]
    Text TieuDeKetQua;

    private Dictionary<string, Text> DicTextKetQuaXoSo;


    private string GiaiDacBiet;
    private string GiaiNhat;

    private string[] ArrayNhi;

    private string[] ArrayBa;

    private string[] ArrayTu;

    private string[] ArrayNam;

    private string[] ArraySau;

    private string[] ArrayBay;


    public string TestSoDanh;
    public int TestLoaiDanh;


    private bool ShowKetQuaThangThua;

    private int TrangGiayDoHienTai;



    private string _DacBiet = "11133";
    private string _Nhat = "33332";
    private string _Nhi = "12222|13332";
    private string _Ba = "12222|33332|44441|55551|66661|77771";
    private string _Tu = "2232|3333|4444|5555";
    private string _Nam = "2222|4333|4444|5532|6666|7777";
    private string _Sau = "232|333|444";
    private string _Bay = "22|32|44|55";

    void Awake()
    {
       // instance = this;

    }

    public void ShowSoKetQua()
    {
        PopupLotteryManager.Panel = "KetQua";

        if (LotteryManager.IsDataReult == true)
        {
            DicTextKetQuaXoSo = new Dictionary<string, Text>();

            PanelKetQua.SetActive(true);

            Vien.SetActive(true);

            PanelThongTinKetQua.SetActive(true);

            PanelThongBaoMatKetNoiMayChu.SetActive(false);

            BtnBack.onClick.RemoveAllListeners();
            BtnBack.onClick.AddListener(OnBtnBackClick);

            BtnExitPanelThongBaoMatKetNoiMayChu.onClick.RemoveAllListeners();
            BtnExitPanelThongBaoMatKetNoiMayChu.onClick.AddListener(BtnExit_1);
            // BtnExit.onClick.RemoveAllListeners();
            // BtnExit.onClick.AddListener(ButtonBackOnClick);



            BtnThangThua.onClick.RemoveAllListeners();
            BtnThangThua.onClick.AddListener(OnPanelThangThua);

            BtnExitThangThua.onClick.RemoveAllListeners();
            BtnExitThangThua.onClick.AddListener(OffPanelThangThua);

            BtnLichSu.onClick.RemoveAllListeners();
            BtnLichSu.onClick.AddListener(BtnLichSuOnClick);



            List_KetQua.SetActive(true);

            TrangGiayDoMacDinh();


            ShowKetQuaThangThua = true;
        }
        else
        {
            OnBtnBackClick();
        }

       
       // DaDatCuoc = true;


      //  SendKetQuaRequest();
       // SendThangThuaRequest();
    }


    public void BtnExit_1()
    {
        LotteryManager.BtnBackOnClick();
    }


    public void TrangGiayDoMacDinh()
    {
        DicTextKetQuaXoSo.Clear();
        for (int i = 0; i < Parent_KetQua.childCount; i++)
        {
            GameObject.Destroy(Parent_KetQua.GetChild(i).gameObject);
        }
        NhanKetQuaXoSoNgayHomNay();

        TestXuatKetQua();
    }



    public void TangTrangGiayDo()
    {
        if (TrangGiayDoHienTai < 5)
        {
            ++TrangGiayDoHienTai;
            string TieuDe = LotteryManager.GetStringDateResult(TrangGiayDoHienTai);
            TieuDeKetQua.text = TieuDe;

            DicTextKetQuaXoSo.Clear();




            for (int i = 0; i < Parent_KetQua.childCount; i++)
            {
                GameObject.Destroy(Parent_KetQua.GetChild(i).gameObject);
            }

            DataResultLottery ketQua = LotteryManager.GetDataResultByDateIndex(TrangGiayDoHienTai);
            _DacBiet = ketQua.DacBiet;
            _Nhat = ketQua.GiaiNhat;
            _Nhi = ketQua.GiaiNhi;
            _Ba = ketQua.GiaiBa;
            _Tu = ketQua.GiaiTu;
            _Nam = ketQua.GiaiNam;
            _Sau = ketQua.GiaiSau;
            _Bay = ketQua.GiaiBay;

            TestXuatKetQua();
        }
       
    }



    public void GiamTrangGiayDo()
    {

        if (TrangGiayDoHienTai > 0)
        {
            --TrangGiayDoHienTai;
            string TieuDe = LotteryManager.GetStringDateResult(TrangGiayDoHienTai);
            TieuDeKetQua.text = TieuDe;

            DicTextKetQuaXoSo.Clear();




            for (int i = 0; i < Parent_KetQua.childCount; i++)
            {
                GameObject.Destroy(Parent_KetQua.GetChild(i).gameObject);
            }

            DataResultLottery ketQua = LotteryManager.GetDataResultByDateIndex(TrangGiayDoHienTai);
            _DacBiet = ketQua.DacBiet;
            _Nhat = ketQua.GiaiNhat;
            _Nhi = ketQua.GiaiNhi;
            _Ba = ketQua.GiaiBa;
            _Tu = ketQua.GiaiTu;
            _Nam = ketQua.GiaiNam;
            _Sau = ketQua.GiaiSau;
            _Bay = ketQua.GiaiBay;

            TestXuatKetQua();
        }

       
    }





    private void NhanKetQuaXoSoNgayHomNay()
    {
        string TieuDe = LotteryManager.GetStringDateResult(0);
        TrangGiayDoHienTai = 0;
        TieuDeKetQua.text = TieuDe;

        

        DataResultLottery ketQua = LotteryManager.GetDataResultByDateIndex(0);
        _DacBiet = ketQua.DacBiet;
        _Nhat = ketQua.GiaiNhat;
        _Nhi = ketQua.GiaiNhi;
        _Ba = ketQua.GiaiBa;
        _Tu = ketQua.GiaiTu;
        _Nam = ketQua.GiaiNam;
        _Sau = ketQua.GiaiSau;
        _Bay = ketQua.GiaiBay;

     
}


    private void SendKetQuaRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.ResultLottery);
        sfs.SendRoomRequest(gp);
    }


    private void SendThangThuaRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.ResultBet);
        sfs.SendRoomRequest(gp);
    }


    /*

    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case CommandKey.ResultLottery:
                NhanDuLieuKetQuaXoSo(gp);
                break;
            case CommandKey.ResultBet:
                NhanKetQuaThangThua(gp);
                break;
           /* case CommandKey.SODATCUOC:
                NhanDuLieuSoDatCuoc(gp);
                break;
        }
    }

        */

    public void NhanKetQuaThangThua(GamePacket gp)
    {

        ISFSArray myArray = gp.GetSFSArray(ParamKey.ResultBet);

       // ShowKetQuaThangThua =

        if (ShowKetQuaThangThua == false)
        {
            List_ThangThua.transform.GetChild(0).gameObject.SetActive(false);
            List_ThangThua.transform.GetChild(1).gameObject.SetActive(false);
            List_ThangThua.transform.GetChild(3).gameObject.SetActive(true);

            Text ThongBao = List_ThangThua.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();

            //if (DaDatCuoc == false)
            {
                ThongBao.text = "HÔM NAY BẠN CHƯA ĐẶT CƯỢC";
            }
            //else
            {
                ThongBao.text = "KẾT QUẢ SẼ CÓ VÀO LÚC 19 GIỜ NGÀY HÔM NAY";
            }
        }
        else
        {
            for (int i = 0; i < myArray.Size(); i++)
            {
                SFSObject obj = (SFSObject)myArray.GetSFSObject(i);
                bool win = obj.GetBool("isWin");

                int indexType = obj.GetInt("Type");
                string LoaiDe = GetStringTypeByIndex(indexType);


                string SoDanh = obj.GetUtfString("BetNumber");

                long TienDat = gp.GetLong("BetMoney");

                long TienThang = gp.GetLong("BetMoneyResult");

                int SoLanXuatHien = gp.GetInt("Count");

                ShowThangThuaDacBiet(i, win, SoDanh, LoaiDe, TienDat.ToString(), TienThang.ToString(), SoLanXuatHien.ToString());


            }
        }

    }

    private string GetStringTypeByIndex(int index)
    {
        string Type = "LÔ 2 SỐ";

        if (index == 1)
        {
            Type = "LÔ 3 SỐ";
        }
        else if (index == 2)
        {
            Type = "3 CÀNG";
        }
        else if (index == 3)
        {
            Type = "ĐỀ ĐẦU";
        }
        else if (index == 4)
        {
            Type = "ĐỀ ĐẶC BIỆT";
        }
        else if (index == 5)
        {
            Type = "ĐÁNH ĐẦU";
        }
        else if (index == 6)
        {
            Type = "ĐÁNH ĐUÔI";
        }

        return Type;
    }


    private int GetIndexByStringType(string Type)
    {
        int index = 0;

        if (Type == "LO 3 SO")
        {
            index = 1;
        }
        else if (Type == "3 CANG")
        {
            index = 2;
        }
        else if ( Type == "DE DAU")
        {
            index = 3;
        }
        else if (Type == "DE DAC BIET")
        {
            index = 4;
        }
        else if (Type == "DANH DAU")
        {
            index = 5;
        }
        else if (Type == "DANH DUOI")
        {
            index = 6;
        }

        return index;
    }


    private void OnPanelThangThua()
    {
        PopupLotteryManager.OnThangThua = true;

        Debug.Log("da vo ne");
        ResetKhongPhaiDangVuaDau();
        PanelThongTinKetQua.SetActive(false);
        Vien.SetActive(false);

        PanelThangThua.SetActive(true);

        List_ThangThua.SetActive(true);
        List_ThangThua.transform.GetChild(0).gameObject.SetActive(true);
        List_ThangThua.transform.GetChild(1).gameObject.SetActive(true);
        List_ThangThua.transform.GetChild(3).gameObject.SetActive(false);


        // TEST
        if (ShowKetQuaThangThua == false)
        {
            List_ThangThua.transform.GetChild(0).gameObject.SetActive(false);
            List_ThangThua.transform.GetChild(1).gameObject.SetActive(false);
            List_ThangThua.transform.GetChild(3).gameObject.SetActive(true);

            Text ThongBao = List_ThangThua.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();

            //if (DaDatCuoc == false)
            {
                ThongBao.text = "HÔM NAY BẠN CHƯA ĐẶT CƯỢC";
            }
            //else
            {
                ThongBao.text = "KẾT QUẢ SẼ CÓ VÀO LÚC 19 GIỜ NGÀY HÔM NAY";
            }
        }
        else
        {
            Debug.Log("da vo ne");

            for (int i = 0; i < Parent_ThangThua.childCount; i++)
            {
                GameObject.Destroy(Parent_ThangThua.GetChild(i).gameObject);
            }

            ShowThangThuaDacBiet(0, true, "61", "LÔ 2 SỐ", "100", "7000", "1");

            ShowThangThuaDacBiet(1, false, "28", "ĐỀ ĐẶC BIỆT", "100", "0", "0");

            ShowThangThuaDacBiet(2, true, "596", "LÔ 3 SỐ", "100", "877000", "1");

            ShowThangThuaDacBiet(3, true, "892", "3 CÀNG", "100", "877000", "1");

            ShowThangThuaDacBiet(4, true, "07", "ĐỀ ĐẦU", "100", "877000", "1");

            ShowThangThuaDacBiet(5, true, "92", "ĐỀ ĐẶC BIỆT", "100", "877000", "1");

            ShowThangThuaDacBiet(6, true, "9", "ĐÁNH ĐẦU", "100", "877000", "1");

            ShowThangThuaDacBiet(7, true, "2", "ĐÁNH ĐUÔI", "100", "877000", "1");

        }

    }


    public void OffPanelThangThua()
    {
        PopupLotteryManager.OnThangThua = false;
        PanelThangThua.SetActive(false);
        PanelThongTinKetQua.SetActive(true);
        Vien.SetActive(true);
    }



    private void TestXuatKetQua()
    {
        XuLyDuLieuKetQuaXoSo(_DacBiet, _Nhat, _Nhi, _Ba, _Tu, _Nam, _Sau, _Bay);
    }


    public void NhanDuLieuKetQuaXoSo(GamePacket gp)
    {
        _DacBiet = gp.GetString("db");
        _Nhat = gp.GetString("1");
        _Nhi = gp.GetString("2");
        _Ba = gp.GetString("3");
        _Tu = gp.GetString("4");
        _Nam = gp.GetString("5");
        _Sau = gp.GetString("6");
        _Bay = gp.GetString("7");


        XuLyDuLieuKetQuaXoSo(_DacBiet, _Nhat, _Nhi, _Ba, _Tu, _Nam, _Sau, _Bay);

    }


    public void XuLyDuLieuKetQuaXoSo(string dacbiet, string nhat, string nhi, string ba, string tu, string nam, string sau, string bay)
    {
        int index = 0;

        GiaiDacBiet = dacbiet.Trim();
        ShowModuleDacBiet(index, "Đặc Biệt", GiaiDacBiet);



        GiaiNhat = nhat.Trim();
        ShowModuleDacBiet(++index, "Giải Nhất", GiaiNhat, false);
        

        ArrayNhi = nhi.Split('|');
        string nhi_1, nhi_2;
        nhi_1 = ArrayNhi[0].Trim();
        nhi_2 = ArrayNhi[1].Trim();
        ShowModuleNhi(++index, "Giải Nhì", nhi_1, nhi_2);



        ArrayBa = ba.Split('|');
        string ba_1, ba_2, ba_3, ba_4, ba_5, ba_6;
        ba_1 = ArrayBa[0].Trim();
        ba_2 = ArrayBa[1].Trim();
        ba_3 = ArrayBa[2].Trim();
        ba_4 = ArrayBa[3].Trim();
        ba_5 = ArrayBa[4].Trim();
        ba_6 = ArrayBa[5].Trim();

        ArrayBa[0] = ba_1;
        ArrayBa[1] = ba_2;
        ArrayBa[2] = ba_3;
        ArrayBa[3] = ba_4;
        ArrayBa[4] = ba_5;
        ArrayBa[5] = ba_6;


        ShowModuleBa(++index, "Giải Ba", ba_1, ba_2, ba_3, ba_4, ba_5, ba_6);



        ArrayTu = tu.Split('|');
        string tu_1, tu_2, tu_3, tu_4;
        tu_1 = ArrayTu[0].Trim();
        tu_2 = ArrayTu[1].Trim();
        tu_3 = ArrayTu[2].Trim();
        tu_4 = ArrayTu[3].Trim();

        ArrayTu[0] = tu_1;
        ArrayTu[1] = tu_2;
        ArrayTu[2] = tu_3;
        ArrayTu[3] = tu_4;

        ShowModuleTu(++index, "Giải Tư", tu_1, tu_2, tu_3, tu_4);





        ArrayNam = nam.Split('|');
        string nam_1, nam_2, nam_3, nam_4, nam_5, nam_6;
        nam_1 = ArrayNam[0].Trim();
        nam_2 = ArrayNam[1].Trim();
        nam_3 = ArrayNam[2].Trim();
        nam_4 = ArrayNam[3].Trim();
        nam_5 = ArrayNam[4].Trim();
        nam_6 = ArrayNam[5].Trim();

        ArrayNam[0] = nam_1;
        ArrayNam[1] = nam_2;
        ArrayNam[2] = nam_3;
        ArrayNam[3] = nam_4;
        ArrayNam[4] = nam_5;
        ArrayNam[5] = nam_6;

        ShowModuleBa(++index, "Giải Năm", nam_1, nam_2, nam_3, nam_4, nam_5, nam_6, false);

        ArraySau = sau.Split('|');
        string sau_1, sau_2, sau_3;
        sau_1 = ArraySau[0].Trim();
        sau_2 = ArraySau[1].Trim();
        sau_3 = ArraySau[2].Trim();

        ArraySau[0] = sau_1;
        ArraySau[1] = sau_2;
        ArraySau[2] = sau_3;


        ShowModuleSau(++index, "Giải Sáu", sau_1, sau_2, sau_3);

        ArrayBay = bay.Split('|');
        string bay_1, bay_2, bay_3, bay_4;
        bay_1 = ArrayBay[0].Trim();
        bay_2 = ArrayBay[1].Trim();
        bay_3 = ArrayBay[2].Trim();
        bay_4 = ArrayBay[3].Trim();

        ArrayBay[0] = bay_1;
        ArrayBay[1] = bay_2;
        ArrayBay[2] = bay_3;
        ArrayBay[3] = bay_4;


        ShowModuleTu(++index, "Giải Bảy", bay_1, bay_2, bay_3, bay_4, false);
/*
        foreach (KeyValuePair<string, Text> item in DicTextKetQuaXoSo)
        {
           
            Debug.Log(item.Key + "------- " + item.Value.text);
        }*/
    }



    public void voiTestTimSoTrung()
    {

        // TimSoTrung(TestSoDanh, TestLoaiDanh);
        ResetKhongPhaiDangVuaDau();
    }


    public void TimSoTrung(string SoDanh,int Type)
    {
        if (Type == 0 || Type == 1 || Type == 3)
        {
            Debug.Log("vo roi ne");
            TimLoaiDe_0(SoDanh, Type);
        }
        else
        {
            TimTrenloDacBiet(SoDanh, Type);
        }
    }



    public void TimTrenloDacBiet(string SoDanh, int Type)
    {
        string CatDacBiet = "";
        if (Type == 2)
        {
            CatDacBiet = GiaiDacBiet.Substring(2, 3);
            if (SoDanh == CatDacBiet)
            {
                KhongPhaiDangVuaDau("db","3");
            }
        }
        else if (Type == 4)
        {
            CatDacBiet = GiaiDacBiet.Substring(3, 2);
            if (SoDanh == CatDacBiet)
            {
                KhongPhaiDangVuaDau("db", "2");
            }
        }
        else if (Type == 5)
        {
            CatDacBiet = GiaiDacBiet.Substring(3, 1);
            if (SoDanh == CatDacBiet)
            {
                KhongPhaiDangVuaDau("db", "dau");
            }
        }
        else if (Type == 6)
        {
            CatDacBiet = GiaiDacBiet.Substring(4, 1);
            if (SoDanh == CatDacBiet)
            {
                KhongPhaiDangVuaDau("db", "duoi");
            }
        }

        

    }







    public void TimLoaiDe_0(string SoDanh, int Type)
    {
        if (Type == 3)
        {
            for (int p = 0; p < ArrayBay.Length; p++)
            {
                string bay = ArrayBay[p];
                if (SoDanh == bay)
                {
                    int a = p + 1;
                    KhongPhaiDangVuaDau("7_" + (a).ToString(), "2");
                }
            }
        }
        else if (Type == 1)
        {
         //   Debug.Log("vo roi ne 1");
            string db = GiaiDacBiet.Substring(2, 3);
            if (SoDanh == db)
            {
                KhongPhaiDangVuaDau("db", "3");
            }
            string nhat = GiaiNhat.Substring(2, 3);
            if (SoDanh == nhat)
            {
                KhongPhaiDangVuaDau("1", "3");
            }

            for (int i = 0; i < ArrayNhi.Length; i++)
            {
                string nhi = ArrayNhi[i].Substring(2, 3);
                if (SoDanh == nhi)
                {
                    int a = i + 1;
                    KhongPhaiDangVuaDau("2_" + (a).ToString(), "3");
                }
            }

            for (int j = 0; j < ArrayBa.Length; j++)
            {
                string ba = ArrayBa[j].Substring(2, 3);
                if (SoDanh == ba)
                {
                    int a = j + 1;
                    KhongPhaiDangVuaDau("3_" + (a).ToString(), "3");
                }
            }

            for (int k = 0; k < ArrayTu.Length; k++)
            {
                string tu = ArrayTu[k].Substring(1, 3);
                if (SoDanh == tu)
                {
                    int a = k + 1;
                    KhongPhaiDangVuaDau("4_" + (a).ToString(), "3");
                }
            }

            for (int l = 0; l < ArrayNam.Length; l++)
            {
               

                string nam = ArrayNam[l].Substring(1, 3);

              
                if (SoDanh == nam)
                {
                    //Debug.Log("vo roi ne 3");
                    int a = l + 1;
                    KhongPhaiDangVuaDau("5_" + (a).ToString(), "3");
                   // Debug.Log("vo roi ne 4");
                }
            }

            for (int t = 0; t < ArraySau.Length; t++)
            {
                string sau = ArraySau[t];
                if (SoDanh == sau)
                {
                    int a = t + 1;
                    KhongPhaiDangVuaDau("6_" + (a).ToString(), "3");
                }
            }
        }
        else if (Type == 0)
        {

            string db = GiaiDacBiet.Substring(3, 2);
            if (SoDanh == db)
            {
                KhongPhaiDangVuaDau("db", "2");
            }
            string nhat = GiaiNhat.Substring(3, 2);
            if (SoDanh == nhat)
            {
                KhongPhaiDangVuaDau("1", "2");
            }

            for (int i = 0; i < ArrayNhi.Length; i++)
            {
                string nhi = ArrayNhi[i].Substring(3, 2);
                if (SoDanh == nhi)
                {
                    int a = i + 1;
                    KhongPhaiDangVuaDau("2_" + (++i).ToString(), "2");
                }
            }

            for (int j = 0; j < ArrayBa.Length; j++)
            {
                string ba = ArrayBa[j].Substring(3, 2);
                Debug.Log("chieu dai---- " + ArrayBa.Length);
                Debug.Log("kkkkkkkkk---- " + ArrayBa[j]);
                Debug.Log("cddddddddd---- " + ArrayBa[j].Length);
                Debug.Log("yyyyyyyyyyy---- " + ba);
                if (SoDanh == ba)
                {
                    int a = j + 1;
                    KhongPhaiDangVuaDau("3_" + (a).ToString(), "2");
                }
            }

           // Debug.Log("222---- " + ArrayBa[1].Length);


            for (int k = 0; k < ArrayTu.Length; k++)
            {
                string tu = ArrayTu[k].Substring(2, 2);
                if (SoDanh == tu)
                {
                    int a = k + 1;
                    KhongPhaiDangVuaDau("4_" + (a).ToString(), "2");
                }
            }

            for (int l = 0; l < ArrayNam.Length; l++)
            {
                string nam = ArrayNam[l].Substring(2, 2);
                if (SoDanh == nam)
                {
                    int a = l + 1;
                    KhongPhaiDangVuaDau("5_" + (a).ToString(), "2");
                }
            }

            for (int t = 0; t < ArraySau.Length; t++)
            {
                string sau = ArraySau[t].Substring(1, 2);
                if (SoDanh == sau)
                {
                    int a = t + 1;
                    KhongPhaiDangVuaDau("6_" + (a).ToString(), "2");
                }
            }

            for (int p = 0; p < ArrayBay.Length; p++)
            {
                string bay = ArrayBay[p];
                if (SoDanh == bay)
                {
                    int a = p + 1;
                    KhongPhaiDangVuaDau("7_" + (a).ToString(), "2");
                }
            }
        }

    }






    private void KhongPhaiDangVuaDau(string key, string type)
    {
        foreach (KeyValuePair<string, Text> item in DicTextKetQuaXoSo)
        {
            if (key == item.Key)
            {
                /*
                Debug.Log("vo roi ne _2");
                item.Value.GetComponent<Text>().color = Color.red;
                item.Value.GetComponent<Text>().fontSize = 45;

                10000000<color=#F02A2AFF>00</color>
                111<color =#F02A2AFF>33</color>

                */
                string txtBanDau = item.Value.text;
                string txtTruoc = "";
                string txtSau = "";
                string txtCuoi = "";
                bool ToanPhan = false;

                if (type == "2")
                {
                    if (txtBanDau.Length > 2)
                    {
                        txtSau = txtBanDau.Substring(txtBanDau.Length - 2, 2);
                        txtTruoc = txtBanDau.Substring(0, txtBanDau.Length - txtSau.Length);
                    }
                    else
                    {
                        ToanPhan = true;
                    }
                   
                }
                else if (type =="3")
                {
                    if (txtBanDau.Length > 3)
                    {
                        txtSau = txtBanDau.Substring(txtBanDau.Length - 3, 3);
                        txtTruoc = txtBanDau.Substring(0, txtBanDau.Length - txtSau.Length);
                    }
                    else
                    {
                        ToanPhan = true;
                    }
                   
                }
                else if (type == "dau")
                {
                    txtTruoc = txtBanDau.Substring(0, 3);
                    txtSau = txtBanDau.Substring(3, 1);
                    txtCuoi = txtBanDau.Substring(4, 1);
                }
                else if (type == "duoi")
                {
                    txtTruoc = txtBanDau.Substring(0, 4);
                    txtSau = txtBanDau.Substring(4, 1);
                }


               
/*
                Debug.Log("******Ban Dau******* " + txtBanDau);
                Debug.Log("******Doan Truoc******* " + txtTruoc);
                Debug.Log("******Doan sau******* " + txtSau);
                Debug.Log("******Doan sau******* " + txtCuoi);
*/
                if (type == "dau")
                {
                    item.Value.text = txtTruoc + "<color=#A500ECFF>" + txtSau + "</color>" + txtCuoi;
                }
                else
                {
                    if (ToanPhan == true)
                    {
                        item.Value.text = "<color=#A500ECFF>" + txtBanDau + "</color>";
                    }
                    else
                    {
                        item.Value.text = txtTruoc + "<color=#A500ECFF>" + txtSau + "</color>";
                    }
                    
                }
            }
        }
    }

    private void ResetKhongPhaiDangVuaDau()
    {

        for (int i = 0; i < Parent_KetQua.childCount; i++)
        {
            GameObject.Destroy(Parent_KetQua.GetChild(i).gameObject);
        }
        DicTextKetQuaXoSo.Clear();
        XuLyDuLieuKetQuaXoSo(_DacBiet, _Nhat, _Nhi, _Ba, _Tu, _Nam, _Sau, _Bay);
    }




    public void BtnLichSuOnClick()
    {
        /*
                PanelKetQua.SetActive(false);
                PanelThangThua.SetActive(false);
                List_ThangThua.SetActive(false);
        **/
        // Debug.Log("123456789");

        PanelKetQua.SetActive(false);
        LotteryManager.OnHistoryButtonClick();
    }



    public void BtnDacBietOnClick()
    {
        ShowModuleDacBiet(0, "Dac Biet", "759");
    }


    public void BtnNhiOnClick()
    {
        ShowModuleNhi(1, "Giai Nhi", "145", "741");
    }


    public void BtnBaOnClick()
    {
        ShowModuleBa(2, "Giai Ba", "142", "45", "0000", "746", "536", "1459");
    }


    public void BtnTuOnClick()
    {
        ShowModuleTu(3, "Giai Tu", "846", "425", "756", "963");
    }


    public void BtnSauOnClick()
    {
        ShowModuleSau(4, "Giai Sau", "84116", "42745", "11756");
    }



    public void Hide()
    {
        //Panel.SetActive(false);
    }


    public void OnBtnBackClick()
    {
        Vien.SetActive(false);
        PanelThongTinKetQua.SetActive(false);
        PanelThangThua.SetActive(false);
        PanelThongBaoMatKetNoiMayChu.SetActive(true);
        //PanelKetQua.SetActive(false);
    }


    void ShowThangThuaDacBiet(int index, bool win, string number, string type, string money, string winmoney, string count)
    {
        ThangThuaModule itemView;
        GameObject obj = Instantiate(ThangThuaModule) as GameObject;
        DataItemBetToDay item = new DataItemBetToDay();
        obj.transform.SetParent(Parent_ThangThua);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;

        //  Color ObjColor = obj.GetComponent<Image>().color;

        /*

        if (win == true)
        {
            obj.GetComponent<Image>().color = Color.green;

        }
        else
        {
            obj.GetComponent<Image>().color = Color.red;
        }
        */
        
        itemView = obj.GetComponent<ThangThuaModule>();
        itemView.Init(item);
        itemView.ShowAfterHaveResult(win, number, type, money, winmoney, count);
    }



    void ShowModuleDacBiet(int index, string type, string number, bool GiaiDacBiet = true)
    {
        KetQuaType itemView;

        GameObject obj = new GameObject();

        if (GiaiDacBiet == true)
        {
            obj = Instantiate(DacBietModule) as GameObject;
        }
        else
        {
            obj = Instantiate(GiaiNhatModule) as GameObject;
        }

        
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_KetQua);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number);

        

        Text txtDacBiet = itemView.Number_1_Txt;
        string key = "";
        if (GiaiDacBiet == true)
        {
            key = "db";
        }
        else
        {
            key = "1";
        }
        Debug.Log("txtDacBiet  " + txtDacBiet);
        DicTextKetQuaXoSo.Add(key, txtDacBiet);
        Debug.Log("het Dac Biet  ");
    }

    void ShowModuleNhi(int index, string type, string number, string numBer_2)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiNhiModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_KetQua);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2);

        Text txtNhi_1 = itemView.Number_1_Txt;
        Text txtNhi_2 = itemView.Number_2_Txt;

        DicTextKetQuaXoSo.Add("2_1", txtNhi_1);
        DicTextKetQuaXoSo.Add("2_2", txtNhi_2); 
    }

    void ShowModuleBa(int index, string type, string number, string numBer_2, string number_3, string number_4, string number_5, string number_6, bool giaiBa = true)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiBaModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_KetQua);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2, number_3, number_4, number_5, number_6);

        Text txtBa_1 = itemView.Number_1_Txt;
        Text txtBa_2 = itemView.Number_2_Txt;
        Text txtBa_3 = itemView.Number_3_Txt;
        Text txtBa_4 = itemView.Number_4_Txt;
        Text txtBa_5 = itemView.Number_5_Txt;
        Text txtBa_6 = itemView.Number_6_Txt;

        string key_1, key_2, key_3, key_4, key_5, key_6;

        if (giaiBa == true)
        {
            key_1 = "3_1";
            key_2 = "3_2";
            key_3 = "3_3";
            key_4 = "3_4";
            key_5 = "3_5";
            key_6 = "3_6";
        }
        else
        {
            key_1 = "5_1";
            key_2 = "5_2";
            key_3 = "5_3";
            key_4 = "5_4";
            key_5 = "5_5";
            key_6 = "5_6";
        }

        DicTextKetQuaXoSo.Add(key_1, txtBa_1);
        DicTextKetQuaXoSo.Add(key_2, txtBa_2);
        DicTextKetQuaXoSo.Add(key_3, txtBa_3);
        DicTextKetQuaXoSo.Add(key_4, txtBa_4);
        DicTextKetQuaXoSo.Add(key_5, txtBa_5);
        DicTextKetQuaXoSo.Add(key_6, txtBa_6);

    }



    void ShowModuleTu(int index, string type, string number, string numBer_2, string number_3, string number_4, bool giaiTu = true)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiTuModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_KetQua);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2, number_3, number_4);

        Text txtTu_1 = itemView.Number_1_Txt;
        Text txtTu_2 = itemView.Number_2_Txt;
        Text txtTu_3 = itemView.Number_3_Txt;
        Text txtTu_4 = itemView.Number_4_Txt;

        string key_1, key_2, key_3, key_4;

        if (giaiTu == true)
        {
            key_1 = "4_1";
            key_2 = "4_2";
            key_3 = "4_3";
            key_4 = "4_4";
        }
        else
        {
            key_1 = "7_1";
            key_2 = "7_2";
            key_3 = "7_3";
            key_4 = "7_4";
        }

        DicTextKetQuaXoSo.Add(key_1, txtTu_1);
        DicTextKetQuaXoSo.Add(key_2, txtTu_2);
        DicTextKetQuaXoSo.Add(key_3, txtTu_3);
        DicTextKetQuaXoSo.Add(key_4, txtTu_4);

    }


    void ShowModuleSau(int index, string type, string number, string numBer_2, string number_3)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiSaModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_KetQua);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2, number_3);

        Text txtSau_1 = itemView.Number_1_Txt;
        Text txtSau_2 = itemView.Number_2_Txt;
        Text txtSau_3 = itemView.Number_3_Txt;

        string key_1, key_2, key_3;

        key_1 = "6_1";
        key_2 = "6_2";
        key_3 = "6_3";

        DicTextKetQuaXoSo.Add(key_1, txtSau_1);
        DicTextKetQuaXoSo.Add(key_2, txtSau_2);
        DicTextKetQuaXoSo.Add(key_3, txtSau_3);

    }
    
}
